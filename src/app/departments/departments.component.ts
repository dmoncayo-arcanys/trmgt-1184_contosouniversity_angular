import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {

  departmentList: any;

  constructor(private oidcSecurityService: OidcSecurityService, private http: HttpClient) { }

  ngOnInit(): void {
    this.getDepartments();
  }

  getDepartments() {
    const token = this.oidcSecurityService.getToken();
    this.departmentList = [];
    this.http.get(Constants.URL_API + '/Department', {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      }),
      responseType: 'json'
    }).subscribe((data: any) => {
      this.departmentList = data.response;
    });
  }

}
