import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.html',
  styleUrls: ['./instructors.component.css']
})
export class InstructorsComponent implements OnInit {

  instructorList: any;

  constructor(private oidcSecurityService: OidcSecurityService, private http: HttpClient) { }

  ngOnInit(): void {
    this.getInstructors();
  }

  getInstructors() {
    const token = this.oidcSecurityService.getToken();
    this.instructorList = [];
    this.http.get(Constants.URL_API + '/Instructors', {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      }),
      responseType: 'json'
    }).subscribe((data: any) => {
      this.instructorList = data.response;
    });
  }

}
