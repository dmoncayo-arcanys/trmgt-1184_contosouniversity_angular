import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AuthModule, LogLevel, OidcConfigService } from 'angular-auth-oidc-client';
import { Constants } from '../commons/Constants'

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { HomeComponent } from './home/home.component';
import { DepartmentsComponent } from './departments/departments.component';
import { InstructorsComponent } from './instructors/instructors.component';
import { CoursesComponent } from './courses/courses.component';

export function configureAuth(oidcConfigService: OidcConfigService) {
  return () => oidcConfigService.withConfig({
      clientId: Constants.CLIENT_ANGULAR,
      stsServer: Constants.URL_SERVER,
      scope: Constants.AUTH_SCOPE,
      redirectUrl: window.location.origin,
      postLogoutRedirectUri: window.location.origin,
      responseType: 'code',
      logLevel: LogLevel.Debug,
  });
}

const routes: Routes = [
  { path: '', component: HomeComponent, },
  { path: 'students', component: StudentsComponent, },
  { path: 'departments', component: DepartmentsComponent, },
  { path: 'instructions', component: InstructorsComponent, },
  { path: 'courses', component: CoursesComponent, },
];

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    HomeComponent,
    DepartmentsComponent,
    InstructorsComponent,
    CoursesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    AuthModule.forRoot(),
  ],
  providers: [
    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configureAuth,
      deps: [OidcConfigService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
