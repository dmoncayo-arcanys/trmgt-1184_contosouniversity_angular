import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  courseList: any;

  constructor(private oidcSecurityService: OidcSecurityService, private http: HttpClient) { }

  ngOnInit(): void {
    this.getCourses();
  }

  getCourses() {
    const token = this.oidcSecurityService.getToken();
    this.courseList = [];
    this.http.get(Constants.URL_API + '/Courses', {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      }),
      responseType: 'json'
    }).subscribe((data: any) => {
      console.log('>>>> ', data);
      this.courseList = data.response;
    });
  }

}
