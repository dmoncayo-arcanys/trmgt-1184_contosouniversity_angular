import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constants } from '../../commons/Constants'

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  studentList: any;

  constructor(private oidcSecurityService: OidcSecurityService, private http: HttpClient) { }

  ngOnInit(): void {
    this.getStudents();
  }

  getStudents() {
    const token = this.oidcSecurityService.getToken();
    this.studentList = [];
    this.http.get(Constants.URL_API + '/Students', {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
      }),
      responseType: 'json'
    }).subscribe((data: any) => {
      this.studentList = data.response;
    });
  }

}
