export class Constants {
    public static CLIENT_ANGULAR = 'client_angular';
    public static URL_SERVER = 'https://localhost:44313';
    public static URL_API = 'https://localhost:44350';
    public static AUTH_SCOPE = 'openid profile ContosoUniversity.API';
}